﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NewTestTask.Models;

namespace NewTestTask.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        TestContext db;
        public HomeController(TestContext context)
        {
            db = context;
        }

        public ActionResult ShowGroupMembers(string GroupName)
        {
            var Members = GetGroupMembers(GroupName);
            return Json(Members);
        }

        protected List<string> GetGroupMembers(string GroupName)
        {
            List<string> Members = new List<string>();
            var gr = db.Groups.SingleOrDefault(s => s.GroupName.ToLower() == GroupName.ToLower());
            if (gr != null)
            {
                Members = gr.UsersInGroups.Select(sel => sel.user.UserName).ToList().Prepend("Пользователи").ToList();
                Members.AddRange(db.Groups.Where(wh => wh.ParentGroup == gr).Select(sel => sel.GroupName).ToList().Prepend("Подгруппы"));
            }
            return Members;
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
