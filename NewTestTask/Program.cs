﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace NewTestTask
{
    public class Program
    {

        //public static void Main(string[] args)
        //{
        //    BuildWebHost(args).Run();
        //}

        //public static IWebHost BuildWebHost(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>()
        //        .Build();

        public static void Main(string[] args)
        {

            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<NewTestTask.Models.TestContext>();
                InitDb(context);

            }

            host.Run();

        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        protected static void InitDb(Models.TestContext db)
        {
            if (!db.Users.Any() && !db.Groups.Any())
            {
                var gr1 = new Models.Group { GroupName = "Group1" };
                var gr2 = new Models.Group { GroupName = "Group2" };
                var gr3 = new Models.Group { GroupName = "Group3", ParentGroup = gr2 };
                var gr4 = new Models.Group { GroupName = "Group4", ParentGroup = gr3 };
                var gr5 = new Models.Group { GroupName = "Group5", ParentGroup = gr2 };

                var usr1 = new Models.User { UserName = "User1" };
                var usr2 = new Models.User { UserName = "User2" };
                var usr3 = new Models.User { UserName = "User3" };
                var usr4 = new Models.User { UserName = "User4" };
                var usr5 = new Models.User { UserName = "User5" };

                gr1.UsersInGroups = new List<Models.UsersInGroups> { new Models.UsersInGroups { group = gr1, user = usr1 }, new Models.UsersInGroups { group = gr1, user = usr5 } };
                gr2.UsersInGroups = new List<Models.UsersInGroups> { new Models.UsersInGroups { group = gr2, user = usr2 } };
                gr3.UsersInGroups = new List<Models.UsersInGroups> { new Models.UsersInGroups { group = gr3, user = usr1 }, new Models.UsersInGroups { group = gr3, user = usr3 } };
                gr4.UsersInGroups = new List<Models.UsersInGroups> { new Models.UsersInGroups { group = gr4, user = usr4 }, new Models.UsersInGroups { group = gr4, user = usr2 } };


                db.Groups.AddRange(gr1, gr2, gr3, gr4, gr5);
                db.Users.AddRange(usr1, usr2, usr3, usr4, usr5);

                 db.SaveChanges();

            }
        }


    }
}
