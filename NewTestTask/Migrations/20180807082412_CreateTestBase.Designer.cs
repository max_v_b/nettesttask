﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NewTestTask.Models;

namespace NewTestTask.Migrations
{
    [DbContext(typeof(TestContext))]
    [Migration("20180807082412_CreateTestBase")]
    partial class CreateTestBase
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("NewTestTask.Models.Group", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("GroupName");

                    b.Property<int?>("Parent_ID");

                    b.HasKey("ID");

                    b.HasIndex("Parent_ID");

                    b.ToTable("Groups");
                });

            modelBuilder.Entity("NewTestTask.Models.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("UserName");

                    b.HasKey("ID");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("NewTestTask.Models.UsersInGroups", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Group_ID");

                    b.Property<int>("User_ID");

                    b.HasKey("ID");

                    b.HasIndex("Group_ID");

                    b.HasIndex("User_ID");

                    b.ToTable("UsersInGroups");
                });

            modelBuilder.Entity("NewTestTask.Models.Group", b =>
                {
                    b.HasOne("NewTestTask.Models.Group", "ParentGroup")
                        .WithMany("ChildGroups")
                        .HasForeignKey("Parent_ID");
                });

            modelBuilder.Entity("NewTestTask.Models.UsersInGroups", b =>
                {
                    b.HasOne("NewTestTask.Models.Group", "group")
                        .WithMany("UsersInGroups")
                        .HasForeignKey("Group_ID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("NewTestTask.Models.User", "user")
                        .WithMany("UsersInGroups")
                        .HasForeignKey("User_ID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
