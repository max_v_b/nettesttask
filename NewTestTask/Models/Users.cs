﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NewTestTask.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<UsersInGroups> UsersInGroups { get; set; }

        //public User()
        //{
        //    UsersInGroups = new List<UsersInGroups>();
        //}
    }
}
