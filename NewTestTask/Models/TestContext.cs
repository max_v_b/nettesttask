﻿using Microsoft.EntityFrameworkCore;

namespace NewTestTask.Models
{
    public class TestContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<UsersInGroups> UsersInGroups { get; set; }

        public TestContext(DbContextOptions<TestContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UsersInGroups>()
                .HasKey(t => new { t.ID });

            modelBuilder.Entity<UsersInGroups>()
                .HasOne(sc => sc.user)
                .WithMany(s => s.UsersInGroups)
                .HasForeignKey(sc => sc.User_ID);

            modelBuilder.Entity<UsersInGroups>()
                .HasOne(sc => sc.group)
                .WithMany(c => c.UsersInGroups)
                .HasForeignKey(sc => sc.Group_ID);
        }


    }
}