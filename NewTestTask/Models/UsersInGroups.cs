﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NewTestTask.Models
{
    public class UsersInGroups
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User user { get; set; }
        public int Group_ID { get; set; }
        [ForeignKey("Group_ID")]
        public virtual Group group { get; set; }

    }
}
