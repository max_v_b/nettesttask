﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NewTestTask.Models
{
    public class Group
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string GroupName { get; set; }
        public int? Parent_ID { get; set; }
        [ForeignKey("Parent_ID")]
        public virtual Group ParentGroup { get; set; }
        public virtual ICollection<Group> ChildGroups { get; set; }
        public virtual ICollection<UsersInGroups> UsersInGroups { get; set; }
        //public Group()
        //{
        //    ChildGroups = new List<Group>();
        //    UsersInGroups = new List<UsersInGroups>();
        //}
    }
}
